//alert("Hello, B176!");

/*
	Objects 
		-is a data type that is used to represent real world objects
		-it is also a collection of related data and/or functionalities

	Syntax:
		let objectName = {
			keyNameA: valueA,
			keyNameB: valueB
		}
*/

//Create an object
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log(cellphone)
console.log(typeof cellphone)

//Creating objects using constructor function
/*
	Constructors are like blueprint or templates to create our objects form

	Syntax:
		function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB
		}

*/

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Dell", 2008)
console.log(laptop)

let laptop2 = new Laptop("Acer", 2020)
console.log(laptop2)

let laptop3 = {
	name: "Asus",
	manufactureDate: 2020,
	price: 2000
}

let laptop4 = ["Asus", 2020]

// function printName(name) {
// 	console.log(`My name is ${name}`)
// }

// printName("Dexter")

//Create empty object
let computer = {}
let myComputer = new Object();
console.log(computer)
console.log(myComputer)

//Accessing Object Properties

//Using the dot notation
//Objectname.propertyName
// laptop = {name: 'Dell', manufactureDate: 2008}
console.log(laptop.name) //Dell
console.log(laptop.manufactureDate)

//Using the square bracket
console.log(laptop["name"])

//Accessing Array Objects
let array = [laptop, laptop2, laptop3]

console.log(array[0]['name'])
console.log(array[0].name)
console.log(array[2].name)

//Initializing/adding/reassigning object properties
let car = {}
console.log(car)

car.name = 'Honda Civic';
console.log(car)

// laptop.price = 20000
// console.log(laptop)

car['manufacture date'] = 2019;
console.log(car);

//delete
delete car['manufacture date'];
console.log(car);

console.log(laptop);
laptop = {
	name: "Toshiba",
	manufactureDate: 2010

}
console.log(laptop);


// car = {

// }

car.name = "Ferrari";

console.log(car);

//sample 2 array email
let user = {
	name: "Dominik",
	email: ["domz@gmail.com", "domz1@gmail.com"]
}


//access
console.log(user);
console.log(user.email[0]);
console.log(user.email);


//Object Methods
// - A method is a function which is a property of an object
// -They are also function and one of the key differences 
// they have is that they are function related to a specific 
// object


//Mini Activity

// Create a function named walk. Print: "Doms walked 25 steps forward"


let person = {
        name:"Dominik",
        talk: function(){
            console.log("Hello my name is " + this.name)
        },
        walk: function(){
            console.log(this.name + " walked 25 steps forward.")
        }
    }
   
    person.talk();
    person.walk();


//Real world application of object

	// Scenario:
	// 1. We would like to create a game that would have
	// several pokemon interact with each other
	// 2.Every pokemon should have the same set of stats,
	// properties and functions


	let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This pokemon tackled targetPokemon")
			console.log("targetPokemon's health is now reduced to targetPokemonHealth")

		},
		faint: function() {
			console.log("Pokemon fainted");
		}
	}

	console.log(myPokemon);


	function Pokemon(name, level) {
	//properties
	
	this.name = name;
	this.level = level;
	this.health = 5 % level;
	this.attack = level

	//methods
	this.tackle = function(target) {

		
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		
		target.health = (target.health - this.attack)

		if(target.health < 5){
			target.faint()
		}

	};
	this.faint = function(targets) {
	
  
  console.log(this.name + " fainted.");

		


	}
}

let pikachu = new Pokemon("Pikachu", 16)
let charizard = new Pokemon("Charizard", 8)
console.log(pikachu)
console.log(charizard)

pikachu.tackle(charizard)
charizard.faint(pikachu)




